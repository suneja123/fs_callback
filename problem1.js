const fs = require("fs");
const path = require("path");
module.exports = function createDir(dirPath, filePath) {

     fs.mkdir(dirPath, { recursive: true }, ((err) => {
        if (err) {
            console.log(err);
        }
        else {
            filePath.forEach(filesPath => {

                const fPath = path.join(dirPath, filesPath)
                fs.writeFile(fPath, "Hey there!", function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("The file is created!");
                    }
                });
            });

            let time = 0;
            filePath.forEach(filesPath => {

                const fPath = path.join(dirPath, filesPath)
                setTimeout(() => {
                    fs.unlink(fPath, function (err) {
                        if (err) console.log(err);
                        // if no error, file has been deleted successfully
                        console.log('File deleted! ' + path.basename(fPath));
                    });
                }, 2000 + time);
                time += 2000;

            });

        }
    }))
}