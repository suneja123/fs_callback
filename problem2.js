
const fs = require("fs");
const path = require("path");
module.exports = function readFile(filePath) {
    try {

        fs.mkdir("./files", { recursive: true }, ((err) => {
            if (err) {
                console.log(err);
            }

            fileReading(filePath, function (err, data) {
                if (err) throw err;
                // console.log(data);
                let stringData = (JSON.stringify(data))

                fileWriting("./files/upperCase.txt", stringData.toUpperCase(), function (err) {
                    if (err) throw err;
                    // console.log('Uppercase conversion completed');

                    fileWriting("./files/fileNames.txt", "upperCase.txt", function (err) {
                        if (err) throw err;

                        fileReading("./files/upperCase.txt", function (err, data) {
                            if (err) throw err;
                            //  console.log(data);
                            let sentences = data.toLowerCase().split(". ");
                            let stringData = JSON.stringify(sentences)

                            fileWriting("./files/lowerCase.txt", stringData, function (err) {
                                if (err) throw err;
                                // console.log('Lower case conversion completed');

                                fs.appendFile("./files/fileNames.txt", "\nlowerCase.txt", function (err) {
                                    if (err) throw err;

                                    fileReading("./files/lowerCase.txt", function (err, data) {
                                        if (err) throw err;
                                        // console.log(data);
                                        let stringData = JSON.parse(data)
                                        let sortedData = stringData.sort();

                                        fileWriting("./files/sorted.txt", sortedData, function (err) {
                                            if (err) throw err;
                                            //console.log("sort completed");

                                            fs.appendFile("./files/fileNames.txt", "\nsorted.txt", function (err) {
                                                if (err) throw err;

                                                //reading directory and deletion
                                                fs.readFile("./files/fileNames.txt", "utf-8", (err, data) => {
                                                    if (err)
                                                        console.log(err);
                                                    else {
                                                        let time = 0;
                                                        let nameOfFiles = data.split("\n");

                                                        nameOfFiles.forEach(file => {


                                                            setTimeout(() => {
                                                                fs.unlink("./files/" + file, function (err) {
                                                                    if (err) console.log(err);
                                                                    // if no error, file has been deleted successfully
                                                                    //console.log('File deleted! ' + path.basename(fPath));
                                                                });
                                                            }, 5000 + time);
                                                            time += 5000;

                                                        })
                                                    }
                                                })
                                            })
                                        })
                                    })

                                })


                            });
                        })


                    })


                });

            });
        }))


    } catch (err) {
        console.error(err);
    }

}
function fileReading(filePath, cb) {
    fs.readFile(filePath, 'utf-8', function (err, data) {
        if (err)
            cb(err, null);
        else {
            cb(null, data);
        }

    })

}
function fileWriting(path, data, cb) {
    fs.writeFile(path, data, function (err) {
        if (err)
            cb(err, null);
        else
            cb(null);
    })

}




